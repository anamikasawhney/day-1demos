﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_Project
{
    internal class Program3
    {
        static void Main()
        {
            int n = 10;
            if (n % 2 == 0) Console.WriteLine("Even");
           else
                Console.WriteLine(  "Odd");
            // ?: Conditional Operator

            Console.WriteLine(n % 2 == 0 ? "Even" : "Odd");

        }
    }
}
