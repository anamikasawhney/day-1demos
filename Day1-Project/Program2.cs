﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_Project
{
    internal class Program2
    {
        static void Main()
        {
            int n1 = 10, n2 = 9;
            int ch = 3;

            switch(ch)
            {
                case 1:
                    Console.WriteLine($"Sum of {n1} and {n2} is {n1 + n2}");
                    break;
                case 2:
                    Console.WriteLine($"Difference of {n1} and {n2} is {n1 - n2}");
                    break;
                case 3:   
                    Console.WriteLine($"Product of {n1} and {n2} is {n1 * n2}");
                    break; 
                case 4:    
                    Console.WriteLine($"Remainder of {n1} and {n2} is {n1 % n2}");
                    break;
                default:
                        Console.WriteLine("Invalid choice");
                    break;

            }
        }
    }
}
