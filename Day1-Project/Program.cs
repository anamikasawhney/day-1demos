﻿////Here System is a namespace, like package in java
//// or like header file in C, C++

//using System;

//namespace Day1_Project
//{
//    internal class Program
//    {
//        static void Main(string[] args)
//        {
//            int n1 = 10;
//            int n2 = 30;
//            Console.WriteLine(n1 + n2);
//            Console.WriteLine("Sum of " + n1 + " and " + n2 + 
//                " is " + (n1+n2));
//            // printf("Sum of %d and %d is %d", n1, n2, n1+n2);
//            Console.WriteLine("Sum of {0} and {1} is {2}",
//                n1 , n2 , n1+n2);
//            // positional parameters
//            // string interpolation
//            Console.WriteLine($"Sum of {n1} and {n2} is {n1 + n2}");
//        }
//    }
//}
// ctrl + KC is to comment statements
// ctrl + KU is to uncomment

using System;
  class Program
{
     static void Main()
    {
        // Operators , Arithmatical
        // + - * % /
        int n1 = 10, n2 = 5;
        // Debug application
        // want to see program flow
        // we can debug our application by first adding a breakpoint.
        // then using F10, F11 keys to continue
        // also we can see the variables values in different windows 
        Console.WriteLine($"Sum of {n1} and {n2} is {n1+n2}");

        Console.WriteLine($"Difference of {n1} and {n2} is {n1 - n2}");

        Console.WriteLine($"Product of {n1} and {n2} is {n1 * n2}");

        Console.WriteLine($"Remainder of {n1} and {n2} is {n1 % n2}");
    }
}
