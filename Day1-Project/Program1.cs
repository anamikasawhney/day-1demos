﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_Project
{
    internal class Program1
    {
        static void Main()
        {
            int n1 = 10, n2 = 9;
            int ch = 3;
            if(ch==1)
                Console.WriteLine($"Sum of {n1} and {n2} is {n1 + n2}");
            else if(ch==2)
            Console.WriteLine($"Difference of {n1} and {n2} is {n1 - n2}");
            else if(ch==3)
            Console.WriteLine($"Product of {n1} and {n2} is {n1 * n2}");
            else if(ch==4)
            Console.WriteLine($"Remainder of {n1} and {n2} is {n1 % n2}");
            else
                Console.WriteLine("Invalid choice");
        }
    }
}

